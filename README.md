# universis-api-stress-test

Stress testing tool for universis api server

# Instructions

1. Create a *.csv file `users.csv` with the following format:

        user1,password,students
        user2,password,students
        ...

where the first column contains the username, the second column is the user's password, and the third column contains a token scope 
e.g. students, teachers or registrar.

This list of users will be used to start scenarios against api server by taking a authorization bearer token for each user.

2. Define client_id and client_secret variables for connecting to OAuth2 server and getting access tokens.

          variables:
            authorize_url: "https://auth.example.com/authorize"
            client_id: 1234567890423432
            client_secret: 6e3956555648576b34576e714c393964

3. Start testing by executing the following command:

        npm run test connect.yaml

A *.csv file will be created under `log` directory which will contain details about requests made by testing process

        student1,"02/24/2020 07:29:57",POST,"https://auth.example.com/authorize",200,-, 107
        student1,"02/24/2020 07:29:57",GET,"https://api.example.com/api/users/me",200,-, 126
        student1,"02/24/2020 07:29:57",GET,"https://api.example.com/api/students/me",200,-, 123
        student1,"02/24/2020 07:29:57",GET,"https://api.example.com/api/students/me/courses",200,-, 232
        student3,"02/24/2020 07:29:59",POST,"https://auth.example.com/authorize",200,-, 90

## Examples

### `test-basic`: 5 users every second for 1 minute (2100 requests)

![Chart](./Duration60ArrivalRate5.png)

### `test-medium`: 5 to 50 users every second for 1 minute (~12000 requests)

![Chart](./Duration120ArrivalRate5RampTo50.png)

