const path = require('path');
const morgan = require('morgan');
const moment = require('moment');
const rfs = require('rotating-file-stream').createStream;

// setup logger directory
let logDir = path.join(process.cwd(), 'log');
// create a rotating write stream
let accessLogStream = rfs('access' + new Date().toISOString() + '.csv', {
    interval: '1d', // rotate daily
    path: logDir
});

// custom morgan tokens
// setup morgan token remote-addr
morgan.token('x-forwarded-for', function (req) {
    return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
});

// setup morgan token date
morgan.token('date', function () {
    return moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
});

// setup morgan token date
morgan.token('time', function () {
    return new Date().getTime();
});

// setup morgan token for user
morgan.token('remote-user', function (req) {
    if (typeof req.context === 'object') {
        if (req.context && req.context.user) {
            return req.context.user.name;
        }
        return 'anonymous';
    }
    return 'unknown'
});

// export access log format
// const ACCESS_LOG_FORMAT = ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time[0] ":referrer" ":user-agent" ":x-forwarded-for"';
const ACCESS_LOG_FORMAT = ':remote-user,":date[web]",:time,:method,":url",:status,:res[content-length], :response-time[0]';

/**
 * Gets application access log handler in order to use it as a middleware in express
 * @returns {RequestHandler}
 */
function accessLogHandler() {
    return morgan(ACCESS_LOG_FORMAT, { stream: accessLogStream,
        // eslint-disable-next-line no-unused-vars
        skip(req, res) {
            return req.method === 'OPTIONS';
        }
    });
}

/**
 * Writes an entry in application access log file based on the given request and response
 * @param {LogRequest} req
 * @param {LogResponse} res
 * @param {*} startAt
 * @param {*} endAt
 */
function writeAccessLog(req, res, startAt, endAt) {
    // get ena time (or now)
    endAt = endAt || process.hrtime();
    // create fake log request
    // these attributes are the required attributes for morgan
    const fakeReq = Object.assign({
        // headers
        headers: req.headers,
        // http method
        method: req.method,
        // url
        url: req.url,
        // remote address
        connection: {
            remoteAddress:  req.connection && req.connection.remoteAddress
        },
        // http version attributes
        httpVersion: '1.1',
        httpVersionMajor: '1',
        httpVersionMinor: '1',
        // this attribute is used by morgan to calculate response time
        _startAt : startAt,
        // finally add context user to log user name
        context: {
            user: (req.context && req.context.user)
        }
    });
    // create fake log response
    let fakeRes = {
        // set headers sent flag
        headersSent: true,
        // set status code
        statusCode: res.statusCode,
        // fake method to avoid morgan errors
        getHeader: () => {
        },
        // this attribute is used by morgan to calculate response time
        _startAt : endAt
    };
    // get formatLine function of morgan
    const formatLine = morgan.compile(ACCESS_LOG_FORMAT);
    // noinspection JSCheckFunctionSignatures
    const line = formatLine(morgan, fakeReq, fakeRes);
    // write access log data to stream
    accessLogStream.write(line + '\n');
}

function loggerBeforeRequest(requestParams, context, ee, next) {
    context.vars.startAt = process.hrtime();
    return next();
}

function loggerAfterResponse(requestParams, response, context, ee, next) {
    const endAt  = process.hrtime();
    writeAccessLog(Object.assign(requestParams, {
        context: {
            user: {
                name: context.vars.username
            }
        }
    }), response, context.vars.startAt, endAt);
    delete context.vars.startAt;
    return next();
}

module.exports = { loggerBeforeRequest, loggerAfterResponse };

